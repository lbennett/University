# Making GitLab Great for Everyone

---

# Our response to the Dear GitHub letter

---

# MR & Issue templates (EE)

- Currently in EE
- Multiple templates in the future
- Templates from the repo soon

---

# Voting on issues

- Fully supported now
- Improved functionality in 8.5
- Voting on comments in the future

---

# Other improvements

- Confidential issues (8.6 EE)
- Deletion of issues (8.6)
- Subscribe to a label (maybe 8.5)
- Subscribe to new issues (8.7)

---

# Better for Enterprise

---

# Working on making HA easier

---

# Significantly improved performance in 8.5
## _Especially for huge servers_

---

# Better design for everyone
